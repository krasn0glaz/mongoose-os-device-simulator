# Mongoose OS devie simulator

This Mac/Linux/Windows program can be used to simulate a real IoT
device for
[device management dashboard](https://mongoose-os.com/docs/userguide/dashboard.md).

## Usage

### Dependencies

Before build you need to install `mbedtls` C library. You will also need `cmake`.

### Installation

#### Build
```bash
git clone gitlab.com/dwarq7/mongoose-os-device-simulator
cd mongoose-os-device-simulator
cd src
make
```

#### Usage


- `./simulator` basic interactive run
- `./simulator -t <token>` run with token
- `./simulator -b <url>` run with URL
- `./simulator -v <verison>` run with version


#### Makefile options

- `run` run binary after build
- `clean` delete binary

## Contributors

- cesanta
- dwarq7